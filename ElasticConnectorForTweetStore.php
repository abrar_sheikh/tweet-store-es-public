<?php
class ElasticConnectorForTweetStore {
    // property declaration
    private $conn;
    private $client;
    private $index;
    private $typeTweets;
    private $date;

    //constants
    private $indexName = "tweetstore";
    private $typeName = "tweetstore";
    private $mappingFile = 'es_tweets_schema.json';
    private $errorLogFile = 'es_error_log.txt';
    private $searchAlias = "ts";
    private $purgeDataOlderThanDays = 3;

    function __construct() {
        require 'vendor/autoload.php';
        $this->connection = new Elastica\Connection();
        $this->client = new Elastica\Client();
        
        $this->setUpIndex();
        return $this;
    }

    private function setUpIndex () {
        $date = date("Y_m_d");
        if(isset($this->date) && $this->date != null && $this->date == $date) 
            return;
        $this->adjustIndex();

        $this->date = $date;
        $this->index =    $this->client->getIndex("{$this->indexName}-$date");
        if(!$this->index->exists())  {
            $this->index->create($this->createIndexTweetStore());
            $this->index->addAlias($this->searchAlias);
        }
        $this->typeTweets =     $this->index->getType($this->typeName);
        if(!$this->typeTweets->exists())  {
            $this->typeTweets->setMapping($this->createTypeTweetsMapping());
        }
    }

    private function adjustIndex() {
        $indices = $this->client->getStatus()->getIndicesWithAlias($this->searchAlias);
        foreach ($indices as $index) {
            $indexName = $index->getName();
            $indexName = explode('-', $indexName);
            if(count($indexName) != 2) 
                return;
            $indexCreationDate = $this->getDaysInterval(str_replace('_', '-', $indexName[1]));
            if ($indexCreationDate >= $this->purgeDataOlderThanDays) {
                $index->delete();
            }
        }
    }

    private function getDaysInterval ($date) {
        $indexCreationDate = strtotime($date);
        $datediff = time() - $indexCreationDate;
        return floor($datediff/(60*60*24));
    }

    private function createTypeTweetsMapping () {
        $mapping = new Elastica\Type\Mapping();
        $mapping->setType($this->typeTweets);

        // Set mapping
        $mapping->setProperties((array)json_decode(file_get_contents($this->mappingFile)));
        $mapping->setParam('dynamic', "strict");
        return $mapping;
    }

    private function createIndexTweetStore() {
        return array("settings" =>array(
            "number_of_shards" => 2,
            "number_of_replicas" => 0,
            'analysis' => array(
                'analyzer' => array(
                    'tweet_analyzer' => array(
                        'type' => 'custom',
                        'tokenizer' => 'whitespace',
                        'filter' => array('lowercase', 'tweet_filter')
                    )
                ),
                'filter' => array(
                    'tweet_filter' => array(
                        'type' => 'word_delimiter',
                        'type_table' => array('# => ALPHA', '@ => ALPHA')
                    )
                )
            )
        ));
    }

    public function bulkInsert($documents) {
        $this->setUpIndex();
        $bulk = new Elastica\Bulk($this->client);
        $bulk->setType($this->typeTweets);
        $bulk->addDocuments($documents);
        $response = $bulk->send();
        if($response->hasError()) {
            //handle error
            $this->log_error($response->getError());
        } 
    }

    public function getDocObj($id, $obj) {
        return new Elastica\Document($id, $obj);
    }

    private function log_error($err) {
        $fp = fopen($this->errorLogFile,'a');
        fwrite($fp, date(DATE_RFC822) . ' | ' . 
            $_SERVER["SCRIPT_NAME"] . ' -> ' . $err. "\n");
        fclose($fp); 
        //mail('', 'tweet-store process error', $msg);
    }
}

?>